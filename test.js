"use strict";

var httpServer = require("./modules/http-server").create({port: 8081, debug: true, allowOrigin: "*"});
var Bluebird = require("bluebird");

httpServer.get("test?test&test2", function(client, respond) {
    console.log(client.query);
    respond(new Bluebird(function(resolve, reject) {
        resolve("ok");
    }));
});

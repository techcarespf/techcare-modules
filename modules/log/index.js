"use strict";

var isNode = (typeof (process) === "object" && Object.prototype.toString.call(process) === "[object process]");

var colors = {
    bold: "1",
    dim: "2",
    italic: "3",
    underline: "4",
    red: "31",
    green: "32",
    yellow: "33",
    blue: "34",
    magenta: "35",
    cyan: "36",
    white: "37",
    grey: "90"
};

var types = {
    log: {name: "log", console: "log", color: "white"},
    debug: {name: "debug", console: "log", color: "grey"},
    info: {name: "info", console: "info", color: "cyan"},
    warn: {name: "warn", console: "warn", color: "yellow"},
    error: {name: "error", console: "error", color: "red"}
};

var colorize = function(string) {
    var args = Array.prototype.slice.call(arguments, 1);
    var colorizedString;

    // If we are on a browser do not colorize
    if (!isNode) {
        return string;
    }

    colorizedString = [string, "\u001b[0m"];

    args.forEach(function(color) {
        if (colors[color]) {
            colorizedString.unshift("\u001b[" + colors[color] + "m");
        }
    });
    return colorizedString.join("");
};

var date = function date() {
    var now = new Date();

    var year = now.getUTCFullYear();
    var month = now.getUTCMonth() + 1;
    var day = now.getUTCDay();
    var hours = now.getUTCHours();
    var minutes = now.getUTCMinutes();
    var seconds = now.getUTCSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hours = (hours < 10 ? "0" : "") + hours;
    minutes = (minutes < 10 ? "0" : "") + minutes;
    seconds = (seconds < 10 ? "0" : "") + seconds;

    return [year, month, day].join("-") + " " + [hours, minutes, seconds].join(":");

};

var log = function log(type) {
    var args = Array.prototype.slice.call(arguments, 1);
    type = types[type];

    // Push date and id into front
    // args.unshift(colorize(id.toString() + ":", "bold", type.color));
    args.unshift(colorize(type.name.toUpperCase() + ":", type.color));
    args.unshift(colorize(date(), "dim", "white"));

    return function() {
        console[type.console].apply(console, args);
    };
};

module.exports = {
    log: log.bind(log, "log"),
    debug: log.bind(log, "debug"),
    info: log.bind(log, "info"),
    warn: log.bind(log, "warn"),
    error: log.bind(log, "error")
};

"use strict";

const urlParser = require("url");
const _http = require("http");
const _https = require("https");

/**
 * Perform an http request
 * @param {string} method The request method (GET, PUT, POST, DELETE)
 * @param {string} url The url of the request
 * @param {string} body The body to send (Only applicable when method is PUT or POST)
 * @returns {Promise.<Object>} The object contains statusCode, headers and body
 */
const requestMethod = function requestMethod(method, url, inBody, headers) {
    return new Promise((resolve, reject) => {
        let options;
        let request;
        let body = inBody;
        let parsedUrl = urlParser.parse(url);

        options = {
            host: parsedUrl.hostname,
            port: parsedUrl.port,
            path: parsedUrl.pathname,
            method: method,
            headers: headers
        };

        if (body && method.match(/^(?:POST|PUT)$/)) {
            options.headers["Content-Type"] = "text/plain";
            if (typeof (body) === "object") {
                options.headers["Content-Type"] = "application/json";
                body = JSON.stringify(body);
                options.headers["Content-Length"] = body.length;
            }
        }

        // Set up the request
        let httpLib = url.match(/^https/) ? _https : _http;

        request = httpLib.request(options, (res) => {
            let chunks = [];
            res.setEncoding("utf8");
            res.on("data", (chunk) => {
                chunks.push((chunk || "").toString("utf8"));
            });
            res.on("end", () => {
                let response = chunks.join("");
                if (res.statusCode < 200 || res.statusCode >= 300) {
                    reject(new Error(response));
                } else {
                    if (res.headers["content-type"] && res.headers["content-type"] === "application/json") {
                        try {
                            response = JSON.parse(response);
                        } catch (e) {
                            console.warn(e);
                        }
                    }
                    resolve({
                        statusCode: res.statusCode,
                        headers: res.headers,
                        body: response
                    });
                }
            });
        });

        // Catch errors
        request.on("error", (err) => {
            reject(err);
        });

        // post the data
        if (method.match(/^(?:POST|PUT)$/) && body) {
            request.write(body);
        }
        request.end();
    });
};

module.exports = {
    post: requestMethod.bind(null, "POST"),
    get: requestMethod.bind(null, "GET"),
    put: requestMethod.bind(null, "PUT")
};

"use strict";

var net = require("net");
var BluebirdPromise = require("bluebird");

var send = function send(host, port, data) {
    return new BluebirdPromise(function(resolve, reject) {
        var client = net.connect({host: host, port: port}, function() {
            client.write(data);
            client.end();
        });

        var chunks = [];

        client.on("data", function(chunk) {
            chunks.push(chunk);
        });

        client.on("end", function() {
            var response = Buffer.concat(chunks).toString("ascii");
            resolve(response);
        });

        client.on("error", function(error) {
            reject(error);
            client.end();
        });
    });
};

module.exports = {
    send: send
};

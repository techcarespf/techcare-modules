"use strict";

var pathUtil = require("path");
var fs = require("fs");
var http = require("http");
var util = require("util");
var EventEmitter = require("events").EventEmitter;
var stream = require("stream");
var urlParser = require("url");

// HELPERS //

/**
 * Check if we have a readable stream
 * @param {*} obj The object to check
 * @returns {boolean} If true then obj is a readable stream
 */
function isReadableStream(obj) {
    return obj instanceof stream.Stream &&
        typeof (obj._read === "function") &&
        typeof (obj._readableState === "object");
}

// Serve a static file
function serveFile(req, respond) {
    var filepath = pathUtil.join(req._route.fileBasePath, req.params.path);
    var headers;
    var fileStream;
    fs.exists(filepath, function(exists) {
        if (!exists) {
            respond(404, "File not found");
        } else {
            fs.stat(filepath, function(err, stats) {
                if (err) {
                    respond(500, err);
                    return;
                }
                headers = {
                    "Content-Length": stats.size
                };
                fileStream = fs.createReadStream(filepath);
                respond(200, fileStream, headers);
            });
        }
    });
}

// Http server class
function HttpServer(options) {
    // Extend options with defaults
    this.options = {
        debug: options.debug || false,
        host: options.host || "0.0.0.0",
        port: parseInt(options.port, 10) || 8080,
        allowOrigin: options.allowOrigin || null
    };

    // Routes
    this.routes = [];

    // Create http server
    this.server = http.createServer(this.request.bind(this));

    // Bind to errors
    this.server.on("error", function(err) {
        this.emit("error", err);
    }.bind(this));

    // Start server
    this.server.listen(this.options.port, this.options.host, function() {
        // Let clients know the server is started
        this.emit("started", "Started @ " + this.options.host + ":" + this.options.port);
    }.bind(this));
}

// Extend event emitter
util.inherits(HttpServer, EventEmitter);

// Get request
HttpServer.prototype.request = function request(req, res) {
    // CORS
    if (this.options.allowOrigin) {
        res.setHeader("Access-Control-Allow-Origin", this.options.allowOrigin);
        res.setHeader("Access-Control-Allow-Headers", "X-Authorization");
        if (req.method === "OPTIONS") {
            res.end();
            return;
        }
    }

    // Get client info
    var client = {
        ip: req.connection.remoteAddress,
        headers: req.headers,
        params: {},
        query: {},
        body: null
    };

    // Remove querystrings
    var url = urlParser.parse(req.url);

    // Try routes
    var routeMatch = this.routes.some(function(route) {
        var match;
        var buffers;

        // Check method
        if (route.method && route.method !== req.method) {
            return false;
        }

        // Check path match
        match = url.pathname.match(route.pattern);
        if (match) {
            // Build query params object
            route.queryParams.forEach(function(queryParam) {
                var m = (url.query || "").match(new RegExp("(?:&|^)" + queryParam + "=([^&]+)(?:&|$)")) || [];
                client.query[queryParam] = m[1] && decodeURIComponent(m[1]) || null;
            });

            // Build params object
            route.params.forEach(function(param, index) {
                client.params[param] = match[index + 1];
            });

            // Read body
            buffers = [];
            req.on("data", function(chunk) {
                buffers.push(chunk);
            });

            req.on("end", function() {
                try {
                    // Build body from chunks
                    client.body = buffers.length ? Buffer.concat(buffers).toString() : null;

                    // Check json
                    if (client.headers["content-type"] && client.headers["content-type"] === "application/json") {
                        client.body = JSON.parse(client.body);
                    }

                    // route to client object
                    client._route = route;

                    // callback
                    route.callback(client, this.respond.bind(this, res));

                } catch (err) {
                    // Respond error
                    this.respond(res, 500, err);
                }
            }.bind(this));

            // TODO: Do some timeout

            // Exit some
            return true;
        }
    }.bind(this));

    // No route match
    if (!routeMatch) {
        res.setHeader("Access-Control-Allow-Origin", this.options.allowOrigin);
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Not found");
    }
};

// Add route
HttpServer.prototype.addRoute = function addRoute(method, path, callback) {
    // Get all callbacks (middleware)
    var callbacks = Array.prototype.slice.call(arguments, 2);

    // Match params in route (e.g. /user/:id/)
    var paramsPattern = /:(.*?)(?:\/|$)/g;
    var matches;
    var params = [];
    var pathPattern;
    var route;
    var queryParams;

    // middleware
    callbacks = callbacks.map(function(cb, i) {
        // Check if last
        if (!callbacks[i + 1]) {
            cb = function(req, next) {

            }
        }


    });

    // Make sure path starts with /
    if (path.substr(0, 1) !== "/") {
        path = "/" + path;
    }

    if (~path.indexOf("?")) {
        queryParams = path.split("?")[1].split("&");
        path = path.split("?")[0];
    }
    while ((matches = paramsPattern.exec(path)) !== null) {
        params.push(matches[1]);
    }

    // Create path regexp pattern
    pathPattern = path;
    params.forEach(function(param) {
        pathPattern = pathPattern.replace(new RegExp(":" + param), "([^/]+?)");
    });
    pathPattern = new RegExp("^" + pathPattern + "$");

    // Add to routes
    route = {
        method: method,
        path: path,
        callback: callback,
        callbacks: callbacks,
        pattern: pathPattern,
        params: params || [],
        queryParams: queryParams || []
    };
    this.routes.push(route);
};

// ALL METHODS (GET, POST, PUT and DELETE)
HttpServer.prototype.all = function all(path, callback) {return this.addRoute(null, path, callback); };

// GET, POST, PUT and DELETE Routes
HttpServer.prototype.get = function get() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift("GET");
    return this.addRoute.apply(this, args);
};
HttpServer.prototype.post = function post() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift("POST");
    return this.addRoute.apply(this, args);
};
HttpServer.prototype.put = function put() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift("PUT");
    return this.addRoute.apply(this, args);
};
HttpServer.prototype.del = function del() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift("DELETE");
    return this.addRoute.apply(this, args);
};

// Add static files path
HttpServer.prototype.serveStatic = function serveStatic(path, filePath) {
    var route;

    // Make sure path starts and ends with "/"
    if (path.substr(-1, 1) !== "/") {
        path = (path.substr(0, 1) !== "/" ? "/" : "") + path + (path.substr(-1, 1) !== "/" ? "/" : "");
    }

    // Add to routes
    route = {
        method: "GET",
        path: path,
        fileBasePath: filePath,
        callback: serveFile,
        pattern: new RegExp(path + "(.+)"),
        params: ["path"]
    };
    this.routes.push(route);
};

// Respond
HttpServer.prototype.respond = function respond(res, statusCode, data, headers) {
    var key;

    // Check for promise
    if (statusCode && typeof (statusCode.then) === "function" && typeof (statusCode.catch) === "function") {
        statusCode
            .then(function(response) {
                this.respond(res, 200, response);
            }.bind(this))
            .catch(function(err) {
                this.respond(res, 500, err);
            }.bind(this));
        return;
    }

    // Set response code
    res.statusCode = statusCode || 200;

    // Set headers
    res.setHeader("X-Server", "NodeJS");
    res.setHeader("Content-Type", "text/plain");
    headers = headers || {};
    for (key in headers) {
        if (headers.hasOwnProperty(key)) {
            res.setHeader(key, headers[key]);
        }
    }

    // Error Object
    if (Object.prototype.toString.call(data) === "[object Error]") {
        data = this.options.debug ? (data.stack || data.message || data) : "Internal server error";

    // Read stram
    } else if (isReadableStream(data)) {
        data.pipe(res);
        return;

    // Object (Assume JSON)
    } else if (typeof (data) === "object") {
        res.setHeader("Content-Type", "application/json");
        data = JSON.stringify(data);
    }

    // End response
    res.setHeader("Content-Length", data.length);
    res.end(data);
};

// Static method to create new instance
HttpServer.create = function create(options) {
    return new HttpServer(options);
};

// Export
module.exports = HttpServer;

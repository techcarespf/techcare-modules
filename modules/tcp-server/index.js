"use strict";

var util = require("util");
var net = require("net");
var EventEmitter = require("events").EventEmitter;


// Tcp server class
var TcpServer = function(options) {
    // Extend options with defaults
    this.options = {
        host: options.host || "0.0.0.0",
        port: parseInt(options.port, 10) || 1337,
        packetSeperator: options.packetSeperator || "\n",
        packetPattern: options.packetPattern || null
    };

    // Packet patterns
    this.patterns = [];

    // Create TCP Server
    this.server = net.createServer(function(stream) {
        // String buffer for incoming data
        var buffer = "";

        // Set stream encoding to ascii
        stream.setEncoding("ascii");

        // Catch error
        stream.on("error", function(err) {
            console.error(err);
        });

        // Incoming data
        stream.on("data", function(data) {
            var offset;
            var packet;
            var patternMatch;

            // Add to buffer
            buffer += data;
            // End on packetSeperator
            offset = buffer.lastIndexOf(this.options.packetSeperator);
            if (offset > -1) {
                packet = buffer.slice(0, offset + 1);
                buffer = buffer.slice(offset + 1);
                packet = packet.replace(this.options.packetSeperator, "");
                patternMatch = this.patterns.some(function(pattern) {
                    var match = packet.match(pattern.pattern);
                    if (match) {
                        pattern.callback(match.slice(1));
                        return true;
                    }
                });

                if (!patternMatch) {
                    this.emit("catchall", packet);
                }

                // Check if stream is still open
                try {
                    stream.write("ok");
                    stream.end();
                } catch(e) {
                    /* nothing */
                }
            }
        }.bind(this));
    }.bind(this));

    // Bind to error
    this.server.on("error", function(err) {
        this.emit("error", err);
    }.bind(this));

    // Start listening
    this.server.listen(this.options.port, this.options.host, function() {
        // Let client know server is started
        this.emit("started", "Started @ " + this.options.host + ":" + this.options.port);
    }.bind(this));
};

// Extend event emitter
util.inherits(TcpServer, EventEmitter);

TcpServer.prototype.onPacket = function onPacket(pattern, callback) {
    this.patterns.push({
        pattern: pattern,
        callback: callback
    });
};

// Static method to create new instance
TcpServer.create = function create(options) {
    return new TcpServer(options);
};

// Export
module.exports = TcpServer;

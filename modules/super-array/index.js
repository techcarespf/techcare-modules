"use strict";

var SuperArray = function SuperArray(initialArray, options) {
    // Options
    this.options = {
        id: (options || {}).id || "id",
        valid_keys: null,
        required_keys: null
    };

    // Set array
    this.array = initialArray || [];
};

// Static method to create a new SuperArray (prettier than using new)
SuperArray.create = function create(initialArray, options) {
    return new SuperArray(initialArray, options);
};

// Property array length
Object.defineProperty(SuperArray.prototype, "length", {
    get: function() {
        return this.array.length;
    }
});

// Property to get set array (set validates input)
Object.defineProperty(SuperArray.prototype, "array", {
    enumerable: false,
    get: function() {
        return this.__array__ || [];
    },
    set: function(array) {
        // Validate
        if (!Array.isArray(array)) {
            throw new Error("Must be an array");
        }
        array.forEach(function(obj) {
            var isInvalid = this.isInvalid(obj);
            if (isInvalid) {
                throw new Error(isInvalid);
            }
        }.bind(this));
        this.__array__ = array;
    }
});

// Returns null (valid) or [message] (invalid)
SuperArray.prototype.isInvalid = function(obj) {
    if (typeof obj !== "object") {
        return "Item is not an object: " + obj;
    } else if (!obj[this.options.id]) {
        return "Item is missing unique field: " + JSON.stringify(obj);
    } else if (this.has(this.options.id, obj[this.options.id])) {
        return "Duplicate item exists with same unique field value" + JSON.stringify(obj);
    }
    return false;
};

SuperArray.prototype.toString = function() {
    return "[object SuperArray]";
};

SuperArray.prototype.valueOf = function() {
    return this.array;
};

SuperArray.prototype.indexOf = function indexOf(value, key) {
    var index = -1;
    key = key || this.options.id;
    if (value === undefined) {
        throw new Error("value is required");
    }
    this.array.some(function(obj, i) {
        if ((obj[key] === undefined ? undefined : obj[key].toString()) === value.toString()) {
            index = i;
            return true;
        }
    });
    return index;
};

SuperArray.prototype.has = function has(value, key) {
    return this.indexOf(value, key) > -1;
};

// Group all values by a key
SuperArray.prototype.groupBy = function groupBy(key) {
    // Throw exception if no key is provided
    if (!key || key === undefined) {
        throw new Error("key is required");
    }
    return this.array.reduce(function(grouped, item) {
        if (!grouped[item[key]]) {
            grouped[item[key]] = [];
        }
        grouped[item[key]].push(item);
        return grouped;
    }, {});
};

// Find all values matching key = value
SuperArray.prototype.find = function find(value, key) {
    key = key || this.options.id;
    if (value === undefined) {
        throw new Error("key is required");
    }
    return this.array.filter(function(obj) {
        return (obj[key] === undefined ? undefined : obj[key].toString()) === value.toString();
    });
};

// Find 1 values matching key = value (throws exception if more than 1 is found and null if 0 found)
SuperArray.prototype.findOne = function findOne(value, key) {
    // vars
    var results;

    // First find all matches
    results = this.find(value, key);

    // Must never return more than 1 result
    if (results.length > 1) {
        throw new Error("findOne should never return more than 1 result");
    }

    // Return match or null
    return results[0] || null;
};

// Get unique key array
SuperArray.prototype.getUnique = function getUnique(key) {
    return this.array.reduce(function(unique, item) {
        if (!~unique.indexOf(item[key])) {
            unique.push(item[key]);
        }
        return unique;
    }, []);
};

// Replace or push obj into array
// Chainable
SuperArray.prototype.replaceOrPush = function replace(obj, key) {
    var index;
    var isInvalid = this.isInvalid(obj);

    // Exception if item is invalid
    if (isInvalid) {
        throw new Error(isInvalid);
    }

    // Default to options.id
    key = key || this.options.id;

    // Get index of item
    index = this.indexOf(key, obj[key]);

    // Item exists
    if (index > -1) {
        this.array[index] = obj;
        return true;
    } else {
        this.array.push(obj);
        return false;
    }
};

// Chainable
SuperArray.prototype.push = function push(obj) {
    var isInvalid = this.isInvalid(obj);
    if (isInvalid) {
        throw new Error(isInvalid);
    }
    this.array.push(obj);
    return this;
};

// Chainable
SuperArray.prototype.sort = function sort(key, key2) {
    this.array.sort(function(a, b) {
        var A = a[key] !== b[key] || !key2 ? a[key] : a[key2];
        var B = a[key] !== b[key] || !key2 ? b[key] : b[key2];
        return A === B ? 0 : (A > B ? 1 : -1);
    });
    return this;
};

// Chainable
SuperArray.prototype.reverse = function reverse() {
    this.array.reverse();
    return this;
};

// Chainable
SuperArray.prototype.randomize = function randomize() {
    var currentIndex = this.length;
    var temporaryValue;
    var randomIndex;
    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = this.array[currentIndex];
        this.array[currentIndex] = this.array[randomIndex];
        this.array[randomIndex] = temporaryValue;
    }
    return this;
};

module.exports = SuperArray;
